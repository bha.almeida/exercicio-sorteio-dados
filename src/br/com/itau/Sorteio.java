package br.com.itau;

import java.util.Random;

public class Sorteio {

    public static int sortearDado(Dado dado) {

        Random random = new Random();
        return random.nextInt(dado.getLados()) + 1;
    }
}
