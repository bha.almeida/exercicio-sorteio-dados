package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Dado {
    private int lados;

    public Dado(int lados) {
        this.lados = lados;
    }

    public int getLados() {
        return lados;
    }

    public void setLados(int lados) {
        this.lados = lados;
    }
}
