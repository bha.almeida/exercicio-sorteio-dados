package br.com.itau;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int qtdeDados = Impressao.solicitarQtdeDados();

        ArrayList resultado = new ArrayList();
        for(int i = 1;i <= qtdeDados;i++){
            Dado dado = new Dado(6);
            resultado.add(Sorteio.sortearDado(dado));
        }
        Impressao.imprimirResultado(resultado);
    }
}
