package br.com.itau;

import java.util.ArrayList;
import java.util.Scanner;

public class Impressao {
    public Impressao() {}

    public static void imprimirResultado(ArrayList resultado){
        System.out.println("Resultado: " + resultado);
    }

    public static int solicitarQtdeDados(){
        System.out.println("Informe a quantidade de dados a serem sorteados:");
        Scanner scanner = new Scanner(System.in);
        int qtdDados = scanner.nextInt();
        return qtdDados;
    }
}
